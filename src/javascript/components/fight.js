import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
    firstFighter.healthPercent = 100 / firstFighter.health;
    secondFighter.healthPercent = 100 / secondFighter.health;

    return new Promise((resolve) => {
        let attacker = null
        let defender = null;
        let healthIndicator = null;
        let fightEnd = false;
        document.addEventListener('keypress', (event) => {
            if (fightEnd) {
                return;
            }

            const keyName = "Key" + event.key.toUpperCase();
            switch (keyName) {
                case controls.PlayerOneAttack:
                    attacker = firstFighter;
                    defender = secondFighter;
                    defender.health -= getDamage(attacker, defender);
                    healthIndicator = document.getElementById('right-fighter-indicator');
                    healthIndicator.style.width = defender.health * defender.healthPercent + '%';
                    break;
                case controls.PlayerTwoAttack:
                    defender = firstFighter;
                    attacker = secondFighter;
                    defender.health -= getDamage(attacker, defender);
                    healthIndicator = document.getElementById('left-fighter-indicator');
                    healthIndicator.style.width = defender.health * defender.healthPercent + '%';
                    break;
            }
               
            if (firstFighter.health <= 0) {
                resolve(secondFighter);
                fightEnd = true;
                healthIndicator.style.width = "0";
            } else if (secondFighter.health <= 0) {
                resolve(firstFighter);
                fightEnd = true;
                healthIndicator.style.width = "0";
            }

        });
  });
}

export function getDamage(attacker, defender) {
    const result = getHitPower(attacker) - getBlockPower(defender);
    return result > 0 ? result : 0;
}

export function getHitPower(fighter) {
    return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
    return fighter.defense * (Math.random() + 1);
}
